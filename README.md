Moems Imageboard
===

Imageboard PHP-project  
---
**Structure:**  
index.php       = Main php index file  
functions.php   = Main function file, to be called in other files.  
config.php      = Main config file, meant for tuning.  
upload.php      = Main upload file, target for upload form.  
dp.php          = Main database file, contains all code related to db.  

Packages/depencies:  
php7  
php7-sqlite  
sqlite  
PDO/PRO-sqlite
  
Uncomment in php.ini  
extension=PDO-sqlite  
extension=sqlite3  
