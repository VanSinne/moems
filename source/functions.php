<?php
// This file is for general functions.

// This section redirects a user without a session
// It should be called in the beginning of all php files.
include 'config.php';
function checksession() { //Function not done yet.
    $earth="flat"; 
    if($earth!="flat"){
        header('Location: index.php');
    }
}


// Inclusion function for universal php files.
function inclusion(){
    $path = getcwd();
    if((file_exists($path."/db.php")) && (file_exists($path."/config.php"))){
    		include 'db.php';
    		include 'config.php';
    }
    else {
     		echo "Code is broken, db.php or functions.php cant be found.";// Code to run if the db.php or functions.php cant be found.
    }
}

// Can be removed, currently used in index.php
function devmode($var) {
    echo '<meta http-equiv="refresh" content="'.$var.'" >';
}

// Redefine scandir -> scan_dir, so that it parses in order of date.
function scan_dir($dir) {
    $ignored = array('.', '..', '.svn', '.htaccess');

    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    return ($files) ? $files : false;
}

// Function to remove files in path older than x.
function rmold($time) { // Time in seconds
    // $time = $time*60*60*24; // Uncomment to make function for days.
  $path = 'images/';
  if ($handle = opendir($path)) {
     while (false !== ($file = readdir($handle))) {
        if ((time()-filectime($path.$file)) >= $time) {  
           //if (preg_match('/\.pdf$/i', $file)) {
            if(($file != ".") && ($file != "..")) {
              unlink($path.$file);
            } 
           //}
        }
     }
   }
}

rmold($image_persistence);

    // Detect content of /images and put them into an array. Used in index
    $path = getcwd()."/".$imagedirectory;
    // Scan directory
        if(is_array(scan_dir($path))) {
            $files = array_values(array_diff(scan_dir($path), array('.', '..')));
    }

?>
