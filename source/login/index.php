<?php
$path = "/";
if((file_exists($path."db.php")) && (file_exists($path."functions.php"))){
		include 'db.php';
		include 'functions.php';
}
else {
		// Code to run if the php.db or functions.php cant be found.
}
?>
<html>
<head><link rel="stylesheet" type="text/css" href="/style.css"></head>
<title>Login</title>
<body>
  <div id="promptwrapper">
    <div class="form">
      <form class="login-form">
        <input type="text" placeholder="username"/>
        <input type="password" placeholder="password"/>
        <button>login</button>
        <p class="message">Not registered? <a href="#">Create an account</a></p>
      </form>
    </div>
  </div>
</body>
</html>
