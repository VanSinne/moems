<?php
// Opening database connection, creating if !exist.
include 'config.php';
$pdo = new PDO("sqlite:$dbname"); // Store database connection (defined in config.php)in a variable $pdo

// Check connection and stop processing code if database is broken (die)...
if(!$pdo){ echo "Connection to database is broken.<br>"; die; }

// Create new table named users if not exist
// $createtable = sql query in plaintext, to be prepared and inserted.
  $createtable = 'CREATE TABLE IF NOT EXISTS users (
  userid INTEGER PRIMARY KEY UNIQUE NOT NULL,
  username TEXT UNIQUE NOT NULL,
  mail TEXT NOT NULL,
  password TEXT NOT NULL
  )';
  $prep = $pdo->prepare($createtable); // Prepare the statement(query)
  $prep->execute(); // Execute the statement(query)


    $createtable = 'CREATE TABLE IF NOT EXISTS images (
    id INTEGER PRIMARY KEY,
    filepath TEXT UNIQUE NOT NULL,
    date TEXT NOT NULL,
    md5sum TEXT NOT NULL
    )';
    $prep = $pdo->prepare($createtable); // Prepare the statement(query)
    $prep->execute(); // Execute the statement(query)


function addimage($filepath, $date, $md5sum, $pdo) {
    $sql = "INSERT INTO images (filepath, date, md5sum) VALUES (?,?,?)";
    $STH = $pdo->prepare($sql);
    $STH->bindParam('1', $filepath);
    $STH->bindParam('2', $date);
    $STH->bindParam('3', $md5sum);
    $STH->execute();
}



// function to add users (for handling registration form) 
function adduser($username, $mail, $password, $pdo) {
  $sql = "INSERT INTO users (username, mail, password) VALUES (?,?,?)";
  $STH = $pdo->prepare($sql);
  $STH->bindParam(1, $username);
  $STH->bindParam(2, $mail);
  $STH->bindParam(3, $password);
  if($username){
      $STH->execute();
  }}
// adduser("knark","asdfasfag","halaljesus", $pdo); // Example-calling for previously defined function.

$stmt = $pdo->query('SELECT mail FROM users WHERE userid = 1');     /////////////// BEST SOLUTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
$fetch = $stmt->fetch();
$knas = $fetch['0'];
// echo $knas; // to call for the previously defined function.

// Detect content of /images and create database entry for every image
$path = getcwd()."/images";
// Scan directory
$files = scandir($path);
$files = array_values(array_diff(scandir($path), array('.', '..')));
?>
