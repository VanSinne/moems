<?php
//if(1+1==3) {
//    $do = "a session checker ";
//}
include 'config.php';
include 'db.php';
include 'functions.php';

// Errormessages...
    $toolarge           = "File is too large. Maximum size is: ".($maximagebytes/1000000)."MB. ";
    $alreadyexist       = "File already exists. ";
    $wrongformat        = "File is wrong format. ";
    $notuploaded        = "Your file was NOT uploaded. ";
    $uploaded           = "Your file was uploaded. ";
    $error              = "Your file passed, but an error occured while uploading. ";
    $nofile             = "No file specified. ";
    $filenotimage       = "Your file is not an image. ";

if(!file_exists($_FILES['fileToUpload']['tmp_name'])) {
    echo $nofile;
    exit();
}

$filename = $_FILES['fileToUpload']['name'];
$filetmp = $_FILES['fileToUpload']['tmp_name'];
$filesize = $_FILES["fileToUpload"]["size"];

$ext = strtolower(pathinfo($filename)['extension']); // Get file extension of the uploaded file.
$md5 = md5_file($filetmp); // Get the md5 hash of the uploaded file.
$md5name = substr($md5, 0, 10); // Take out the 10 first letters of the md5 hash, suitable for a file name.
$target_file = $imagedirectory."/".$md5name.".".$ext; // Declaring target file based on $imagedirectory (declared in config file)
$boolok = true; // Bool var, if this is 1 at the end of the script, the image has met all the requirements and will be uploaded.

if(isset($_POST["submit"])) {
    $check = getimagesize($filetmp);
    if($check==false){
        echo $filenotimage;
        $boolok = false;
    } 
}

// If already exist
if(file_exists($target_file)) {  // The first condition may be removed in the future.
    echo $alreadyexist;
    $boolok = false;
}

// Filesize limit
if($filesize > $maximagebytes){
    echo $toolarge;
    $boolok = false;
}

// Format checking, validformats declare in config.php.
if(!in_array($ext,$validformats)){
    echo $wrongformat;
    $boolok = false;
}

// Check $boolok, any errors would have resulted in 0. 
if (!$boolok) {
    echo $notuploaded;
} else { // If $boolok still is true -> upload file.
    if (move_uploaded_file($filetmp, $target_file)) {
        echo $uploaded;
        if(file_exists($target_file)) {
            // Add to database
            addimage($target_file, time(), $md5, $pdo); // Insert file path, unix time, and md5sum
        }
        header("Location: index.php");
        exit;
    } else {
        echo $error;
    }
}
?>

