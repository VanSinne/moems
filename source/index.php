<?php
// Inclusions for different types of functions;
if((file_exists(getcwd()."/functions.php"))) {
    include 'functions.php';
        //rmold(5);
    //include 'db.php';
    inclusion(); // predefined in functions.php, loads all the other php files.
}
else {
    echo "Site is broken, functions.php could not be found..."; die;
}
//devmode(10); // Defined in functions.php, updates html every x seconds.
?>

<head>
<title>Möms</title>
<!--link rel="shortcut icon" href="media/icon.ico"-->
<link rel="shortcut icon" href="media/micon.ico">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<html>
<body>
<div id="header">
<div id="logo"><img src="media/micon2.png" style="visibility: hidden;" /></div>
<div id="upload_button"></div>
</div>
<?php // echo "Images persists for: ".($image_persistence/60/60/24)." days." ;?>
<div id="wrapper">
    <main>
        <?php
        //print_r($files);
        if(isset($files)) {
            foreach($files as $row) {
                echo '<img src="images/'.$row.'" id="image" draggable="false" onclick="onClick(this)">';
        }}
        else {
            echo "There are no images to display... ";
        }
            echo '<!-- The Modal -->
            <div id="myModal" class="modal">
              <span class="close">&times;</span>
              <img class="modal-content" draggable="false" id="img01">
              <div id="caption"></div>
            </div>';
        ?>
        <div id="upload_dialog">
            <div id="upload" onclick="event.stopPropagation(); upload_click();">
                <form action="upload.php" method="post" enctype="multipart/form-data">
                    Select image to upload:
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <input type="submit" value="Upload Image" name="submit">
                </form>
            </div>
        </div>
<script type="text/javascript" src="jsfunctions.js"></script>
<!-- The script section above needs to be called here for some reason...
if the script is included earlier in the script, the close funcitons wont work.-->
    </main>
</div>
</body>
<script>
</html>
